CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers


 INTRODUCTION
 ------------
 This module allows you to assign slugs to facet values for Entity Reference
 fields to replace the node ID in the URL.

 Currently, when you have an Entityreference field as a facet, even when using
 Pretty Paths each facet value will be an NID, which is not very pretty. This
 module adds a new coder for Facet API Pretty Paths, along with an admin
 settings page, which allows you to create slugs for your reference facets!


  * For a full description of the module, visit the project page:
    https://www.drupal.org/sandbox/bmcclure/2479847


  * To submit bug reports and feature suggestions, or to track changes:
    https://www.drupal.org/project/issues/2479847


REQUIREMENTS
------------
This module requires the following modules:
 * Facet API (https://www.drupal.org/project/facetapi)
 * Facet API Pretty Paths (https://www.drupal.org/project/facetapi_pretty_paths)
 * Entityreference (https://www.drupal.org/project/entityreference):
   Note that other reference types might work, and you can easily extend this
   module to support any other type


RECOMMENDED MODULES
-------------------
 * Search API (https://www.drupal.org/project/search_api):
   Ideal for displaying your slugged facets in Views
 * Computed Field (https://www.drupal.org/project/computed_field):
   A great way to create a slug field for your content types
 * Pathauto (https://www.drupal.org/project/pathauto):
   Contains the pathauto_cleanstring function which can be very useful for
   creating slugs using Computed Field


INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------
 * Customize the module's settings in Configuration � Search and metadata �
   Facet API Pretty Paths � Pretty Paths Slugs.


TROUBLESHOOTING
---------------
 * If the slugs are not working, check the following:


   - Have you enabled slugging functionality and selected the right slug field
     and facets on the configuration page?

   - Have you added the selected slug field to the appropriate content types
     and populated it with text slugs?

   - Have you tried clearing your cache?


FAQ
---
Q: How should I create slug fields?

A: I recommend creating a field of type "Computed" named "Slug" with a machine
   name "field_slug" and utilizing Pathauto's "pathauto_cleanstring" function
   to generate the slug.


Q: My items are slugged, but the wrong value is being used for one or more
   slugs. How do I fix this?

A: This can happen if one or more of your slugs is not unique. Each slug
   should be unique across ALL content to allow this module to find the correct
   node for a slug.


MAINTAINERS
-----------
Current maintainers:
 * Ben McClure (bmcclure) - https://drupal.org/user/278485


This project has been sponsored by:
 * Top Floor Technologies
   Internet marketing and web development specialists. Visit
   http://www.topfloortech.com for more information.
