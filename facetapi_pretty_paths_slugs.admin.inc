<?php
/**
 * @file
 * Administration page callbacks for the Menu Item View module.
 */

/**
 * Get the field types that should be offered as possible slug fields.
 *
 * By default, these are "text" and "computed."
 *
 * You can add or modify values by calling your own
 * hook_facetapi_pretty_paths_slugs_allowed_field_types_alter function
 *
 * @return array
 *   Return an array of field types.
 */
function facetapi_pretty_paths_slugs_allowed_field_types() {
  $allowed_field_types = &drupal_static(__FUNCTION__);

  if (!isset($allowed_field_types)) {
    $allowed_field_types = array('text', 'computed');

    drupal_alter('facetapi_pretty_paths_slugs_allowed_field_types', $allowed_field_types);
  }

  return $allowed_field_types;
}

/**
 * Get the specific fields which should never be offered as slug values.
 *
 * By default, only the "body" field is specifically excluded.
 *
 * Add or modify these values by calling your own
 * hook_facetapi_pretty_paths_slugs_exclude_fields_alter function.
 *
 * @return array
 *   Return an array of field excludes.
 */
function facetapi_pretty_paths_slugs_exclude_fields() {
  $exclude_fields = &drupal_static(__FUNCTION__);

  if (!isset($exclude_fields)) {
    $exclude_fields = array('body');

    drupal_alter('facetapi_pretty_paths_slugs_exclude_fields', $exclude_fields);
  }

  return $exclude_fields;
}

/**
 * Get a list of supported facet types to offer the user to select for slugging.
 *
 * By default only "node" facet types are supported.
 *
 * Add or modify values by calling your own
 * hook_facetapi_pretty_paths_slugs_facet_types_alter function.
 *
 * @return array
 *   Return an array of allowed facet types.
 */
function facetapi_pretty_paths_slugs_facet_types() {
  $facet_types = &drupal_static(__FUNCTION__);

  if (!isset($facet_types)) {
    $facet_types = array('node');

    drupal_alter('facetapi_pretty_paths_slugs_facet_types', $facet_types);
  }

  return $facet_types;
}

/**
 * Get a list of all possible slug fields sans unsupported and excluded fields.
 *
 * @internal
 *
 * @return array
 *   Return an array of available fields for use as a select list.
 */
function _facetapi_pretty_paths_slugs_get_available_fields() {
  $allowed_field_types = facetapi_pretty_paths_slugs_allowed_field_types();

  $exclude_fields = facetapi_pretty_paths_slugs_exclude_fields();

  $available_fields = array();
  foreach (field_info_fields() as $field) {
    if (!in_array($field['type'], $allowed_field_types) ||
      in_array($field['field_name'], $exclude_fields)) {
      continue;
    }

    $available_fields[$field['field_name']] = $field['field_name'];
  }

  return $available_fields;
}

/**
 * Gets all available facets for all searchers sans unsupported facet types.
 *
 * @internal
 *
 * @return array
 *   Return an array of available facets for use as a select list.
 */
function _facetapi_pretty_paths_slugs_get_available_facets() {
  $facet_types = facetapi_pretty_paths_slugs_facet_types();

  $searchers = facetapi_get_searcher_info();

  $available_facets = array();

  foreach ($searchers as $searcher) {
    $facets = facetapi_get_facet_info($searcher['name']);

    foreach ($facets as $facet) {
      if (!in_array($facet['field type'], $facet_types)) {
        continue;
      }

      $available_facets[$searcher['name'] . '|' . $facet['name']] = $searcher['label'] . ' - ' . $facet['label'];
    }
  }

  return $available_facets;
}

/**
 * Return a render array for an admin settings form.
 *
 * @ingroup forms
 *
 * @see system_settings_form()
 */
function facetapi_pretty_paths_slugs_admin_settings() {
  $form = array();

  $form['facetapi_pretty_paths_slugs_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable slugging functionality for all entity reference facets'),
    '#description' => t('If checked, all entity references will use the slug field if present.'),
    '#default_value' => variable_get('facetapi_pretty_paths_slugs_enabled', TRUE),
  );

  $form['facetapi_pretty_paths_slugs_slug_field'] = array(
    '#type' => 'select',
    '#title' => t('Slug field'),
    '#description' => t('Slugs will be loaded from this field on referenced entities if it exists.'),
    '#options' => _facetapi_pretty_paths_slugs_get_available_fields(),
    '#default_value' => variable_get("facetapi_pretty_paths_slugs_slug_field", NULL),
  );

  $form['facetapi_pretty_paths_slugs_enabled_facets'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Enabled facets'),
    '#description' => t('Slugs will be used for the checked facets.'),
    '#options' => _facetapi_pretty_paths_slugs_get_available_facets(),
    '#default_value' => variable_get("facetapi_pretty_paths_slugs_enabled_facets", array()),
  );

  return system_settings_form($form);
}
