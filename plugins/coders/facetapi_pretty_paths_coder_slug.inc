<?php
/**
 * @file
 * A coder for pretty paths that utilizes slugs.
 */

/**
 * Slug implementation of FacetApiPrettyPathsCoder.
 */
class FacetApiPrettyPathsCoderSlug extends FacetApiPrettyPathsCoderDefault {

  /**
   * Slug special case: <alias>/<slug>.
   *
   * @param array $args
   *   Path segment arguments.
   *
   * @return array
   *   Return the parent's encoded path segment.
   *
   * @see FacetApiPrettyPathsCoderDefault::encodePathSegment()
   */
  public function encodePathSegment(array $args) {
    $field = variable_get("facetapi_pretty_paths_slugs_slug_field", NULL);

    if (!empty($field)) {
      if ($entity = entity_load_single('node', $args['segment']['value'])) {
        $items = field_get_items('node', $entity, $field);

        if (is_array($items)) {
          $args['segment']['value'] = $items[0]['value'];
        }
      }
    }

    return parent::encodePathSegment($args);
  }

  /**
   * Slug special case: <alias>/<slug>.
   *
   * @param array $args
   *   Path segment arguments.
   *
   * @return array
   *   Return parent's decoded path segment.
   *
   * @see FacetApiPrettyPathsCoderDefault::decodePathSegmentValue()
   */
  public function decodePathSegmentValue(array $args) {
    $field = variable_get("facetapi_pretty_paths_slugs_slug_field", NULL);

    if (!empty($field)) {
      $query = new EntityFieldQuery();

      $query->entityCondition('entity_type', 'node')
        ->fieldCondition($field, 'value', $args['value'], '=');

      $result = $query->execute();

      if (!empty($result['node'])) {
        $nids = array_keys($result['node']);

        $args['value'] = $nids[0];
      }
    }

    return parent::decodePathSegmentValue($args);
  }

}
